import {useState, useEffect} from "react"
import styled from "styled-components"
import Maps from "../component/maps"

const Container = styled.div`
  max-width: 480px;
  width: 100%;
  margin: 0 auto;
  height: 100vh;
`
const WelcomeImg = styled.div`
  position: relative;
  background: url("/1.jpg") no-repeat center;
  background-size: cover;
  width: 100%;
  height: 100vh;
  div {
    padding-top: 70px;
    p:first-child {
      font-size: 20px;
      letter-spacing: 3px;
      color: #fff;
      margin: 0;
      font-family: georgia;
      text-align: center;
    }
    p:last-child {
      font-size: 60px;
      font-family: garden;
      color: #fff;
      text-align: center;
      margin: 35px 0 0;
    }
  }
`
const PartyDate = styled.img`
  position: absolute;
  left: 50%;
  bottom: 25px;
  transform: translateX(-50%);
  z-index: 100;
  width: 150px;
`
const Introduction = styled.div`
  width: 100%;
  padding: 25px 20px 0;
`
const IntroTitle = styled.p`
  font-family: gothicb;
  font-size: 30px;
  color: #000000;
  margin: 0;
  text-align: center;
`
const IntroImg = styled.img`
  width: 100%;
  margin: 25px 0 0;
`
const IntroUser = styled.p`
  font-size: 60px;
  font-family: garden;
  color: #000;
  text-align: center;
  margin: 25px 0 0;
`
const IntroUserDesc = styled.p`
  font-size: 14px;
  line-height: 20px;
  font-family: gothic;
  color: #000;
  text-align: center;
  margin: 7px 0 0;
`
const SocialMedia = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 7px 0;
  img {
    width: 24px;
    height: 24px;
  }
  p {
    font-family: gothic;
    font-size: 20px;
    line-height: 30px;
    color: #000;
    font-style: italic;
    margin: 0 3px;
  }
`
const Underline = styled.img`
  width: 100%;
`
const IntroDesc = styled.p`
  font-family: gothicb;
  font-size: 18px;
  color: #000;
  text-align: center;
`
const IntroDate = styled.p`
  font-size: 80px;
  color: #000;
  font-family: gothicb;
  margin: 0;
  text-align: center;
`
const Address = styled.img`
  width: 100%;
  margin-top: 25px;
`
const Information = styled.div`
  background: #000;
  width: 100%;
  padding: 25px 25px 40px;
`
const InfoTitle = styled.a`
  font-size: 26px;
  color: #fff;
  font-family: gothicb;
  display: flex;
  justify-content: center;
`
const CountDown = styled.p`
  font-size: 60px;
  font-family: gothicb;
  color: #fff;
  text-align: center;
  margin: 25px 0 0;
`
const Gallery = styled.div`
  background: #feeed7;
  width: 100%;
  height: 100vh;
  padding: 25px;
`
const GalleryTitle = styled.p`
  margin: 0;
  font-size: 26px;
  color: #000;
  font-family: gothicb;
  text-align: center;
  letter-spacing: 1px;
`

function MainPage() {
  const [countDown, setCountDown] = useState("00.00.00.00")
  useEffect(() => {
    const countDownDate = new Date("Nov 1, 2020 09:00:00").getTime()

    setInterval(function () {
      const now = new Date().getTime()
      const distance = countDownDate - now
      let days = Math.floor(distance / (1000 * 60 * 60 * 24))
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      )
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
      let seconds = Math.floor((distance % (1000 * 60)) / 1000)
      days = days < 10 ? `0${days}` : days
      hours = hours < 10 ? `0${hours}` : hours
      minutes = minutes < 10 ? `0${minutes}` : minutes
      seconds = seconds < 10 ? `0${seconds}` : seconds
      setCountDown(days + "." + hours + "." + minutes + "." + seconds)
      if (distance < 0) {
        setCountDown("00.00.00.00")
        return
      }
    }, 1000)
  }, [])
  return (
    <Container>
      <WelcomeImg>
        <div>
          <p>UNDANGAN PERNIKAHAN</p>
          <p>Shanty & Fikrie Zul</p>
        </div>
        <PartyDate src="/date.png" alt="date" />
      </WelcomeImg>
      <Introduction>
        <IntroTitle>Perkenankan Kami:</IntroTitle>
        <IntroImg src="/2.jpg" alt="intro" />
        <IntroUser>Shanty Nur Rizky</IntroUser>
        <IntroUserDesc>
          Putri pertama dari Bapak Banisah & Ibu Tati Haryati
        </IntroUserDesc>
        <SocialMedia>
          <img src="/ig.png" alt="ig" />
          <p>@shantynurrizky</p>
        </SocialMedia>
        <IntroUser style={{margin: "10px 0"}}>&</IntroUser>
        <IntroUser style={{margin: 0}}>Zulpikri Rangkuti</IntroUser>
        <IntroUserDesc>
          Putra pertama dari Bapak M. Yunus Rangkuti & Ibu Masta Tanjung
        </IntroUserDesc>
        <SocialMedia>
          <img src="/ig.png" alt="ig" />
          <p>@fikriezul</p>
        </SocialMedia>
        <Underline src="/underline.jpg" alt="underline" />
        <IntroDesc>
          Mengundang keluarga & rekan-rekan untuk hadir dan berbagi kegembiraan
          dalam acara pernikahan kami yang akan dilangsungkan pada tanggal:
        </IntroDesc>
        <IntroDate>01.11.20</IntroDate>
        <Address src="/address.png" alt="address" />
        <Underline
          src="/underline-2.jpg"
          alt="underline"
          style={{margin: "10px 0 30px"}}
        />
      </Introduction>
      <Information>
        <InfoTitle>SIMPAN TANGGAL</InfoTitle>
        <CountDown>{countDown}</CountDown>
        <InfoTitle style={{margin: "50px 0 10px"}}>LOKASI ACARA</InfoTitle>
        <Maps />
      </Information>
      <Gallery>
        <GalleryTitle>FOTO GALERI</GalleryTitle>
      </Gallery>
    </Container>
  )
}

export default MainPage
