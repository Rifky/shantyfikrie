import {createGlobalStyle} from "styled-components"

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  * {
    box-sizing: border-box;
  }
  @font-face {
    font-family: georgia;
    src: url('/fonts/georgia.woff');
  }
  @font-face {
    font-family: gothic;
    src: url('/fonts/gothic.woff');
  }
  @font-face {
    font-family: gothicb;
    src: url('/fonts/gothicb.woff');
  }
  @font-face {
    font-family: garden;
    src: url('/fonts/garden.woff');
  }
`

export default function App({Component, pageProps}) {
  return (
    <>
      <GlobalStyle />
      <Component {...pageProps} />
    </>
  )
}
