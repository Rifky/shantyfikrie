module.exports = {
  assetPrefix:
    process.env.NODE_ENV === "production" ? "/<GITLAB_PROJECT_NAME>" : ""
}
