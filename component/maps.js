import {useState, useEffect} from "react"
import styled from "styled-components"
import GoogleMapReact from "google-map-react"

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 18px;
  height: 18px;
  background-color: #000;
  border: 2px solid #fff;
  border-radius: 100%;
  user-select: none;
  transform: translate(-50%, -50%);
  cursor: ${(props) => (props.onClick ? "pointer" : "default")};
  &:hover {
    z-index: 1;
  }
`
const Contain = styled.div`
  width: 100%;
  height: 250px;
  border-radius: 6px;
`

const Marker = ({text, onClick}) => <Wrapper alt={text} onClick={onClick} />

Marker.defaultProps = {
  onClick: null
}

const LOS_ANGELES = [-6.705403, 108.5532272]
export default function Home() {
  const [places, setPlaces] = useState([])

  const fetchPlaces = async () => {
    fetch("places.json")
      .then((response) => response.json())
      .then((data) => setPlaces(data.results))
  }

  useEffect(() => {
    fetchPlaces()
  }, [])

  if (!places || places.length === 0) {
    return null
  }
  return (
    <Contain>
      <GoogleMapReact defaultZoom={10} defaultCenter={LOS_ANGELES}>
        {places.map((place) => (
          <Marker
            key={place.id}
            text={place.name}
            lat={place.geometry.location.lat}
            lng={place.geometry.location.lng}
          />
        ))}
      </GoogleMapReact>
    </Contain>
  )
}
